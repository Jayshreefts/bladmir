package com.federox.bladmir_expert;

public class Blog {

    private String drname,zone,region,hq,vedioUrl,image;

    public Blog(String drname, String zone, String region, String hq, String vedioUrl, String image) {
        this.drname = drname;
        this.zone = zone;
        this.region = region;
        this.hq = hq;
        this.vedioUrl = vedioUrl;
        this.image = image;
    }
    public Blog(){

    }

    public String getDrname() {
        return drname;
    }

    public void setDrname(String drname) {
        this.drname = drname;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getHq() {
        return hq;
    }

    public void setHq(String hq) {
        this.hq = hq;
    }

    public String getVedioUrl() {
        return vedioUrl;
    }

    public void setVedioUrl(String vedioUrl) {
        this.vedioUrl = vedioUrl;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
