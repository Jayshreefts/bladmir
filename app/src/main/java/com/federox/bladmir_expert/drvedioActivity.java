package com.federox.bladmir_expert;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class drvedioActivity extends AppCompatActivity {
    private String mPost_key=null;
    private DatabaseReference mDatabase;

    private VideoView mainVedio;
    private TextView mdoctrname,mzone,mregion,mhq;

    private Uri videoUri;

    private StorageReference videoRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drvedio);


        mDatabase= FirebaseDatabase.getInstance().getReference().child("Blog");


        StorageReference storageRef =
                FirebaseStorage.getInstance().getReference();

        videoRef = storageRef.child("/vedio/" + mPost_key + "userIntro.3gp");


        mPost_key=getIntent().getExtras().getString("blog_id");
///       Toast.makeText(doctorlistActivity.this,post_key,Toast.LENGTH_LONG).show();



        mdoctrname=(TextView)findViewById(R.id.post_mdrname1);
        mzone=(TextView) findViewById(R.id.post_mzone1);
        mregion=(TextView) findViewById(R.id.mpost_region1);
        mhq=(TextView) findViewById(R.id.mpost_hq1);
        mainVedio=(VideoView)findViewById(R.id.main_vedio);

//        videoUri=Uri.parse("https://firebasestorage.googleapis.com/v0/b/bladmirexpert.appspot.com/o/Jayant%20Patel%20Ahmedabad.m4v.mp4?alt=media&token=08aa201c-5ab5-4d80-ae15-b81566ace587");


        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String post_drname=(String)dataSnapshot.child("drname").getValue();
                String post_zone=(String)dataSnapshot.child("zone").getValue();
                String post_region=(String)dataSnapshot.child("region").getValue();
                String post_hq=(String)dataSnapshot.child("hq").getValue();
                String post_video=(String)dataSnapshot.child("vedio").getValue();
                String post_uid=(String)dataSnapshot.child("uid").getValue();

                mdoctrname.setText(post_drname);
                mzone.setText(post_zone);
                mregion.setText(post_region);
                mhq.setText(post_hq);


                mainVedio.setVideoURI(Uri.parse(post_video));

                mainVedio.requestFocus();
                mainVedio.start();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });







    }


}
