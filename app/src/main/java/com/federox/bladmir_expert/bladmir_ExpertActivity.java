package com.federox.bladmir_expert;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class bladmir_ExpertActivity extends AppCompatActivity {


private ImageButton slectimg;

private EditText mdrname;
    private EditText mzone;
    private EditText mregion;
    private EditText mHq;
    private EditText mvedio;

    private Button madd;
    private StorageReference mstorage;
    private DatabaseReference mdatabase;
    private ProgressDialog mdialog;


   private Uri imageUri=null;

    private static final int Gallary_Request=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bladmir__expert);


        mstorage= FirebaseStorage.getInstance().getReference();
        mdatabase= FirebaseDatabase.getInstance().getReference().child("Blog");

         slectimg=(ImageButton)findViewById(R.id.nav_slectimg);
        mdrname=(EditText) findViewById(R.id.nav_drname);
        mzone=(EditText) findViewById(R.id.nav_zone);
        mregion=(EditText) findViewById(R.id.nav_region);
        mHq=(EditText) findViewById(R.id.nav_hq);
        mvedio=(EditText) findViewById(R.id.nav_vedio);
        madd=(Button) findViewById(R.id.nav_add);

        mdialog=new ProgressDialog(this);

        madd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             startposting();
            }
        });
        slectimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent galleryIntent=new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent,Gallary_Request);

            }
        });




    }

    private void startposting() {
         mdialog.setMessage("Posting to Blog......");
         mdialog.show();
        final String drname=mdrname.getText().toString().trim();
        final String zone=mzone.getText().toString().trim();
        final String region=mregion.getText().toString().trim();
        final String hq=mHq.getText().toString().trim();
        final String vedio=mvedio.getText().toString().trim();

        if(!TextUtils.isEmpty(drname)&& !TextUtils.isEmpty(zone)&& !TextUtils.isEmpty(region)&& !TextUtils.isEmpty(hq)&& !TextUtils.isEmpty(vedio)&&imageUri!=null){

            mdialog.show();

           StorageReference filepath=mstorage.child("Blog_Image").child(imageUri.getLastPathSegment());

           filepath.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
               @Override
               public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                   Uri downloadUri=taskSnapshot.getDownloadUrl();

                   DatabaseReference mpost=mdatabase.push();
                   mpost.child("drname").setValue(drname);
                   mpost.child("zone").setValue(zone);
                   mpost.child("region").setValue(region);
                   mpost.child("hq").setValue(hq);

                   mpost.child("vedio").setValue(vedio);
                   mpost.child("image").setValue(downloadUri.toString());

                   mdialog.dismiss();
                   startActivity(new Intent(bladmir_ExpertActivity.this,listActivity.class));

               }
           });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==Gallary_Request && resultCode==RESULT_OK){
            imageUri=data.getData();


            slectimg.setImageURI(imageUri);
        }

    }
}
