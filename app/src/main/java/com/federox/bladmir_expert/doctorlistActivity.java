package com.federox.bladmir_expert;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class doctorlistActivity extends AppCompatActivity {

    private String mPost_key=null;
    private DatabaseReference mDatabase;
    private Button vedio;
    private ImageView doctrimage;
    private TextView mdoctrname,mzone,mregion,mhq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctorlist);


        mDatabase= FirebaseDatabase.getInstance().getReference().child("Blog");



        mPost_key=getIntent().getExtras().getString("blog_id");
///       Toast.makeText(doctorlistActivity.this,post_key,Toast.LENGTH_LONG).show();



        mdoctrname=(TextView)findViewById(R.id.post_mdrname);
        mzone=(TextView) findViewById(R.id.post_mzone);
        mregion=(TextView) findViewById(R.id.mpost_region);
        mhq=(TextView) findViewById(R.id.mpost_hq);
        doctrimage=(ImageView) findViewById(R.id.post_image1);
        vedio=(Button)findViewById(R.id.nav_vedio);
       vedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Intent doctrlist=new Intent(doctorlistActivity.this,drvedioActivity.class);
                doctrlist.putExtra("blog_id",mPost_key);
                startActivity(doctrlist);
            }
        });



        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String post_drname=(String)dataSnapshot.child("drname").getValue();
                String post_zone=(String)dataSnapshot.child("zone").getValue();
                String post_region=(String)dataSnapshot.child("region").getValue();
                String post_hq=(String)dataSnapshot.child("hq").getValue();
                String post_image=(String) dataSnapshot.child("image").getValue();
                String post_uid=(String)dataSnapshot.child("uid").getValue();

                mdoctrname.setText(post_drname);
                mzone.setText(post_zone);
                mregion.setText(post_region);
                mhq.setText(post_hq);

                Picasso.with(doctorlistActivity.this).load(post_image).into(doctrimage);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




    }

}
