package com.federox.bladmir_expert;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class mapActivity extends AppCompatActivity {
     ImageButton imgbtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        imgbtn=(ImageButton)findViewById(R.id.imgbtn1);
        imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mimg=new Intent(mapActivity.this,listActivity.class);
                startActivity(mimg);
            }
        });
    }
}
