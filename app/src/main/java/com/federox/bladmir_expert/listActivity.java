package com.federox.bladmir_expert;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static java.util.Locale.filter;

public class listActivity extends AppCompatActivity {

    private DatabaseReference ndatabase;
    private FirebaseRecyclerAdapter firebaseRecyclerAdapter;
    private RecyclerView mbloglist;
    private ImageButton imgbtn;
    EditText editTextSearch;
    SearchView searchView;

    LinearLayout linert;
//    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listactivity);
//        imgbtn=(ImageButton)findViewById(R.id.imgbtnsearch) ;
        editTextSearch = (EditText) findViewById(R.id.editTextSearch);
        ndatabase= FirebaseDatabase.getInstance().getReference().child("Blog");
        mbloglist=(RecyclerView)findViewById(R.id.id_blog);
        mbloglist.setHasFixedSize(true);
        mbloglist.setLayoutManager(new LinearLayoutManager(this));
        linert=(LinearLayout) findViewById(R.id.liniear);

         imgbtn=(ImageButton) findViewById(R.id.imagebtn);
        String searchText=editTextSearch.getText().toString();
        onStart(searchText);

        //searching command
        editTextSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String searchText=editTextSearch.getText().toString();

                //where is your code to call firebase at load
//               Query queryRef = ndatabase.orderByChild("drname").startAt(searchText);
                onStart(searchText);


            }
        });

    }

    /*private void firebaseusersSearch(String searchText) {
//    Toast.makeText(listActivity.this,"Started search",Toast.LENGTH_LONG).show();
        Query firebasesearchQuery=ndatabase.orderByChild("drname").startAt(searchText).endAt(searchText+"\uf8ff");

    }*/


    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item){

        if(item.getItemId()==R.id.menu_add){
            startActivity(new Intent(listActivity.this,bladmir_ExpertActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    protected void onStart(String searchText) {

//        super.onStart();

        Query firebasesearchQuery=ndatabase.orderByChild("drname").startAt(searchText).endAt(searchText+"\uf8ff");



        FirebaseRecyclerAdapter<Blog,BlogViewHolder> firebaseRecyclerAdapter=new FirebaseRecyclerAdapter<Blog,BlogViewHolder>(

             Blog.class,
                R.layout.list_detail_layout,
                BlogViewHolder.class,
//                 ndatabase,
                firebasesearchQuery

        ){
            protected void populateViewHolder(BlogViewHolder viewHolder,Blog model,int position){

                final String post_key=getRef(position).getKey();

                viewHolder.setDrname(model.getDrname());

                viewHolder.setRegion(model.getRegion());
                viewHolder.setZone(model.getZone());
                viewHolder.setHq(model.getHq());
                viewHolder.setImage(getApplicationContext(),model.getImage());


                viewHolder.mview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        Toast.makeText(listActivity.this,post_key,Toast.LENGTH_LONG).show();
                      Intent doctrlist=new Intent(listActivity.this,doctorlistActivity.class);
                      doctrlist.putExtra("blog_id",post_key);
                      startActivity(doctrlist);


                    }
                });
            }
        };
        mbloglist.setAdapter(firebaseRecyclerAdapter);

    }




    public static class BlogViewHolder extends RecyclerView.ViewHolder{
        View mview;
        private List<Blog> drname;
        public BlogViewHolder( View itemView) {
            super(itemView);
            mview=itemView;


        }




        public void setDrname(String drname){

            TextView post_name=(TextView)mview.findViewById(R.id.post_name);
            post_name.setText(drname);

        }
       public void setZone(String zone){

            TextView post_zone=(TextView)mview.findViewById(R.id.post_zone);
            post_zone.setText(zone);

        }
           public void setRegion(String region){

              TextView post_region=(TextView)mview.findViewById(R.id.post_region);
              post_region.setText(region);

          }
          public void setHq(String hq){

              TextView post_hq=(TextView)mview.findViewById(R.id.post_hq);
              post_hq.setText(hq);

          }
        public void setImage(Context ctx, String image){
            ImageView post_image=(ImageView)mview.findViewById(R.id.nav_postimg);
            Picasso.with(ctx).load(image).into(post_image);

        }




    }

}
